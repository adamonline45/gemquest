// Jewel.h

#ifndef CJEWEL_H
#define CJEWEL_H

class Jewel
{
    private:
        char avatar;
        int color;
        

        
    public:
        
        char getAvatar() { return avatar; }
        int getColor() { return color; }
        
        void setColor(int);
        void setAvatar(char);
        
        void build();
        
        void populate(int);
    
        // Constructor
        Jewel();
        Jewel(int, char);
};

#endif
