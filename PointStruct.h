// PointStruct.h (no .cpp for this one)

#ifndef POINT_STRUCT_H
#define POINT_STRUCT_H

struct PointStruct
{
    int color;
    int value;
    int multiplier;
    char avatar;
    
    int frame;
    
    bool pooped;
    
    PointStruct::PointStruct()
    {
        frame = 0;
        color = 8;
        avatar = 48; // obvious if something errs
    }
    
    PointStruct::PointStruct(int val, int col, char av, int mult)
    {
        setInfo(val, col, av, mult);
    }
    
    void PointStruct::setInfo(int newValue, int newColor, char newAvatar, int newMultiplier)
    {
        frame = 0;
        
        color = newColor;
        value = newValue;
        multiplier = newMultiplier;
        avatar = newAvatar;
    }
    
    void PointStruct::update()
    {
        //setcolor(5,6);

        if (frame < 8) ++frame;
        //cout << frame;        

        switch(frame)
        {
            case 0:
                // no change;
                break;
            case 1:
                // no change;
                break;
            case 2:
                color -= 8; // darken color
                break;
            case 3:
                // no change
                break;
            case 4:
                // no change;
                break;
            case 5:
                color = 8; // dark gray
                break;
            case 6:
                color = 0; // black
                break;
            case 7:
                value = 0;
                break;
            default:
                break;

        }
        
    }
    
};

#endif

