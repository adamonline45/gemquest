// GLOBAL_GAME_DATA.h

// This is a SINGLETON PATTERN

#ifndef GLOBAL_GAME_DATA_H
#define GLOBAL_GAME_DATA_H

//#define INIT_GAME_DATA		GLOBAL_GAME_DATA *GAME_DATA = GLOBAL_GAME_DATA::getInstance();
//#define INIT_GAME_DATA_RO	const GLOBAL_GAME_DATA GAME_DATA

class GLOBAL_GAME_DATA
{
	private:
		// A private constructor
		GLOBAL_GAME_DATA();		

		// The sole instance
		static GLOBAL_GAME_DATA *instance;

	public:
		
		// Prototype for getInstance() function
		static GLOBAL_GAME_DATA* getInstance();

		// BEGIN items of non-singleton criticality, AKA game data
		bool paused;

		int height;
		int width;

		short boardOffsetX;
		short boardOffsetY;

		short timerOffsetX;
		short timerOffsetY;

		short pointOffsetX;
		short pointOffsetY;
		short pointWidth;
		short pointHeight;

		void setOffsets(int x, int y) { boardOffsetX = x; boardOffsetY = y; }

		bool preClearing;
		//bool suspendGraphicOutput;
		bool showingInstructions;

		bool showPoints; // Whether or not to show the scoreboard

		void setDimensions(int w, int h) { width = w; height = h; }
};

#endif
