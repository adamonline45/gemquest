// common.h

#ifndef COMMON_H
#define COMMON_H

#include <iostream>
#include <windows.h>

using std::cout;
using std::cin;

inline void gotoxy(int x, int y)
{
   COORD cursor_pos;
   cursor_pos.X = x;
   cursor_pos.Y = y;
   SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), cursor_pos);
}

inline void setcolor(int fcolor, int bcolor = 0)
{
	//this function sets the color of the console output
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),(WORD)((bcolor << 4) | fcolor ));
}

void inline waitForEnter()
{
	setcolor(7,0);
	gotoxy(25,23);
	cout << "Press <enter> to continue";
	cin.ignore();
}

void inline clearWaitForEnter()
{
	setcolor(7,0);
	gotoxy(25,23);
	cout << "                         ";
}

#endif
