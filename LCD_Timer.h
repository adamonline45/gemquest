// LCD_Timer.h

#ifndef LCD_TIMER_H
#define LCD_TIMER_H

#include <iostream>
#include <conio.h>
#include <windows.h>

#include "Timer.h"
#include "common.h"
#include "GLOBAL_GAME_DATA.h"

using namespace std;




class LCD_Timer
{
    private:
		int offsetY, offsetX;
		int minutes;
		int seconds;
		bool stopped;

		int fgcolor;

		// the 3x3 characters
		static const char characters[10][3][3];

		Timer *timer;
		Timer * colorTimer;

		GLOBAL_GAME_DATA *GAME_DATA;

    public:
        // Constructor
        LCD_Timer();
		LCD_Timer(int, int, int, int, int);
		~LCD_Timer();
        
        void draw();
		void update();
		void stop() { stopped = true; }
		void start() { stopped = false; }
		void setCoords(int x, int y) { offsetX = x; offsetY = y; }
		void setPeriod(int period) { timer->setPeriod(period); }

		int getTimeLeft() { return (minutes * 60 + seconds); }

    
		// if time's run out, this == true
		bool expired;
    
    
};

#endif