// Timer.cpp

#include <time.h>
#include <iostream>

using namespace std;

#include "Timer.h"

Timer::Timer()
{
    oldTime = clock();
    period = 50;
}

void Timer::setPeriod(long input)
{
    period = input;
}

Timer::Timer(long period)
{
    this->period = period;
    oldTime = clock();
}

bool Timer::signal()
{
    currentTime = clock();
    if (currentTime >= oldTime + period)
    {
        oldTime = currentTime;
        return true;
    }
    else return false;
}
