// GLOBAL_GAME_DATA.cpp

#include "GLOBAL_GAME_DATA.h"

GLOBAL_GAME_DATA::GLOBAL_GAME_DATA()
{
	// Set default info
	paused = false;
	height = 0;
	width = 0;

	showPoints = true;

	boardOffsetX = 0;
	boardOffsetY = 0;

	timerOffsetX = 41;
	timerOffsetY = 2;

	pointOffsetX = 40;
	pointOffsetY = timerOffsetY + 4; // 4 is the timer height
	pointHeight = 20;
	pointWidth = 12;

	showingInstructions = false;
}

GLOBAL_GAME_DATA* GLOBAL_GAME_DATA::instance = 0;

GLOBAL_GAME_DATA* GLOBAL_GAME_DATA::getInstance()
{
	if (!instance) instance = new GLOBAL_GAME_DATA;
	return instance;
}
