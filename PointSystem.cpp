// PointSystem.cpp

#include "PointSystem.h"


PointSystem::PointSystem()
{
	GAME_DATA = GLOBAL_GAME_DATA::getInstance();

	offsetX = GAME_DATA->pointOffsetX;
	offsetY = GAME_DATA->pointOffsetY;
	width = GAME_DATA->pointWidth;
	//height = GAME_DATA->pointHeight; // See LIST_SIZE, defined in .h
    
    latest = 0;
    total = 0;
    
	(GAME_DATA->showPoints) ? doUpdate = true : doUpdate = false;
    
    for (int x = 0; x < LIST_SIZE; ++x)
    {
        list[x].value = 0;
    }
    
    // Set the score timer's period...
    scoreTimer.setPeriod(50); // Make pointer to use overloaded constructor?
    
    // border only needs to be drawn once...
    char tb = 223; // tob border
    char bb = 220; // bottom border
            
    setcolor(15,0);
    
	if (GAME_DATA->showPoints)
	{
		gotoxy(offsetX, offsetY );
		for (int x = 0; x < width; ++x) cout << tb;
		//cout << (char)219;
	    
		gotoxy(offsetX, offsetY + LIST_SIZE + 1);
		for (int x = 0; x < width; ++x) cout << tb;
		//cout << (char)219;
	    
		gotoxy(offsetX, offsetY + LIST_SIZE + 3);
		for (int x = 0; x < width; ++x) cout << bb;
		//cout << (char)219;
	}

    
}

// This is a slot for receiving signals from Space::Clear (or was it Player::clear?)
void PointSystem::importQueue(queue<PointStruct> newQueue)
{
    short iterations = newQueue.size();
    for (int x = 0; x < iterations; ++x)
    {
        pointQueue.push( newQueue.front() );
        newQueue.pop();
    }
    doUpdate = true;
}

void PointSystem::update()
{
    //static int consecutiveUpdates;
    if ( scoreTimer.signal() )
    {
        // Update half-time (for things that only update every other frame)
        (halfTime)? halfTime = false: halfTime = true;
            
        if (doUpdate) // every period , from here down updates...
        {

            if ( pointQueue.size() > 0 )
            {
                if (list[LIST_SIZE - 1].value == 0)
                {
                    list[LIST_SIZE - 1] = pointQueue.front();
                    pointQueue.pop();
                }
    
            }
            
            int updateChecker = 0;
                for (int x = 0; x < LIST_SIZE; ++x)
                {            
                    //list[x].update();
                    setcolor(list[x].color,0);
                    gotoxy(offsetX, offsetY + LIST_SIZE - x); // Draw from the top down
                    
                    if (list[x].value > 0)
                    {
                        ++updateChecker; // keep updating as long as >=1 item has a positive value
                        
                        for (int y = 0; y < list[x].value; ++y)
                        {
                            cout << ' ' << list[x].avatar;
                        }
                        if (list[x].multiplier > 1)
                        {
                            gotoxy(offsetX + 8, offsetY + LIST_SIZE - x);
                            cout << " x" << list[x].multiplier << "          ";
                        }
                        else for (int x = 0; x < width-1; ++x) cout << ' ';
                    }
                    else for (int x = 0; x < width-1; ++x) cout << ' ';
                }
            if ( updateChecker <= 0 ) doUpdate = false;
            
            
            if (list[0].value > 0)
            {
                // Add the last points if there are any
                total += list[0].value * list[0].multiplier;
            }
            
            // Shift the whole list
            for (int x = 0; x < LIST_SIZE -1; ++x)
            {
                // overwrite list[0]
                list[x] = list[x + 1];
            }
            // and set the last element to 0
            list[LIST_SIZE - 1].value = 0; // set the last element to be replaceable...

            setcolor(7,0);
            gotoxy(offsetX, offsetY + LIST_SIZE + 2);
            cout << ' ' << total;    
        }

        if (halfTime)
        {
			// Half-speed updates, should they be needed...
        }
    }
}
