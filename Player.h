// Player.h

#ifndef PLAYER_H
#define PLAYER_H

#include "Space.h"
#include "common.h"
#include "Timer.h"
#include "PointSystem.h"
#include "GLOBAL_GAME_DATA.h"
#include "LCD_Timer.h"

//class GLOBAL_GAME_DATA;
class Player
{
	
    private:
		GLOBAL_GAME_DATA *GAME_DATA;

        Space **spaces;
        
		LCD_Timer *lcd_timer;

        Timer animationTimer;
        
        PointSystem *pointSystem;
        
        int posX;
        int posY;
        
        int oldPosY;
        int oldPosX;
        
        int selectedPosX;
        int selectedPosY;
        
        //int points;

        bool updating;
        bool lockSelections;
        bool animating;
        bool clearing;
        bool dropping;
        bool won;
		bool lost;
        
        int currentFrame;
        
    public:
        Player();
		~Player();
        
        void move(int, int);
        void select();
        
        bool checkForMatches();
        void clearSpaces();
        void dropSpaces();
		void preClear();
		void prepareForIntro();
        
        void update();
        
        void playerWin();
		void playerLost();
        bool checkForWin();
		bool checkForLoss();
        
        bool hasWon() { return won; }
		bool hasLost() { return lost; }

		int getScore() { return pointSystem->getPoints(); }

		bool isUpdating() { return updating; }

		// Cheat
		void instawin()
		{
			for (int y = 0; y < GAME_DATA->height; ++y)
			{
				for (int x = 0; x < GAME_DATA->width; ++x)
				{
					spaces[y][x].setCleared(true);
				}
			}
			checkForWin();
		}
};

#endif
