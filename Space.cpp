// Space.cpp

#include <iostream>
using namespace std;

#include <windows.h>

#include "Space.h"
#include "common.h"

// Static var declaration
int Space::incrementer = 0;
int Space::matchColor = 0;
int Space::matchCount = 0;
int Space::multiplier = 1;
char Space::matchAvatar = 49;
queue<PointStruct> Space::pointQueue;

Space::Space()
{
	GAME_DATA = GLOBAL_GAME_DATA::getInstance();

    highlighted = false;
    selected = false;
    cleared = false;
    matched = false;
    empty = false;

    coordX = (incrementer % GAME_DATA->width) * 3 + 1 + GAME_DATA->boardOffsetX;
    coordY = (incrementer / GAME_DATA->width) * 2 + 1 + GAME_DATA->boardOffsetY;
    incrementer++;
    
    //draw(); // Can't draw yet; requires all surrounding space colors to be
              // defined...
    
    west = NULL;
    east = NULL;
    south = NULL;
    north = NULL;
}


void Space::swap(Space & that)
{
    // Swap jewels
    Jewel buffer = that.jewel;
    that.jewel = this->jewel;
    this->jewel = buffer;
    
    // Deselect both
    this->selected = false;
    that.selected = false;
    
    // Highlight this, un-highlight that
    this->highlighted = true;
    that.highlighted = false;
    
    // Redraw both
    that.draw();
    this->draw();
}

/*************************
/*
/*      BEGIN UPDATE
/*
/*************************/

bool Space::drop()
{    
    if ( south && south->isEmpty() )
    {
        // If this is a top space, build a new jewel before passin it down...
        if (!north && isEmpty() )
        {
            jewel.build();
            setEmpty(false);
            if (!GAME_DATA->preClearing) draw();
        }
        
        if (!isEmpty())
        {
            south->jewel = jewel;
            setEmpty(true);
            south->setEmpty(false);
            if (!GAME_DATA->preClearing) south->draw();
            
            clear(6);
        }

        
        
        return true;
    }
    
    // Fill an empty top space even if there is not a blank below it...
    if ( !north && isEmpty() )
    {
        jewel.build();
        setEmpty(false);
        if (!GAME_DATA->preClearing) draw();
        
        
        return true;
    }
    
    return false;
    
}

/*************************
/*
/*      UPDATE-RELATED
/*        FUNCTIONS
/*
/*************************/

int Space::clear(int frame)
{
    if (matched)
    {
		if (!GAME_DATA->preClearing)
		{
			// These two should be static - How to declare static array?
			// If dynamically, how to erase it only upon deconstruction of last space?
			// Even necessary to delete?  Ask Sensei...
			char frames[] = {42,15,42,43,249,250};
			int frameCount = 6; // size of frames[]

			if (frame < frameCount && frame != -1)
			{
				jewel.setAvatar(frames[frame]);
				draw();
			}
			else // If we're done animating
			{
				//cout << "x";
				setMatched(false);
				setEmpty(true);
				setCleared(true); // been cleared at least once...
				//setColor(0);
				draw();
	            
				return 1;
			}
		}
		else // If this is a pre-clear
		{
			setMatched(false);
			setEmpty(true);
			return 1;
		}
    }
    
    return 0;
    
}

/*************************
/*
/*      END UPDATE
/*
/*************************/

queue<PointStruct> Space::exportQueue()
{
    // copy point queue so we can delete it before returning it...
    queue<PointStruct> tempQueue;
    short iterations = pointQueue.size();

    //cout << pointQueue.size() << ' ' << tempQueue.size();
    
    for (int x = 0; x < iterations; ++x)
    {
        tempQueue.push( pointQueue.front() );
        pointQueue.pop(); // COULD change to .clear()...
    }
    
    multiplier = 1;
    
    //cout << pointQueue.size() << ' ' << tempQueue.size();
    //system("PAUSE");
    
    return tempQueue;
}

bool Space::checkEastForMatches()
{
    PointStruct tempPointStruct;
    /*
    // debug
    int oldColor = getColor();
    setColor(15);
    draw();
    Sleep(15);
    setColor(oldColor);
    draw();
    */
    bool returnValue = false;
    
    if (getColor() != matchColor)
    {
		if (matchCount >= 3 && !GAME_DATA->preClearing) // If so, update point queue for transfer to player's point system
        {
            PointStruct tempPointStruct(matchCount, matchColor, matchAvatar, multiplier);
            pointQueue.push(tempPointStruct);
            ++multiplier;
        }
        
        matchCount = 1;
        matchColor = getColor(); // this also sets the first one...
        matchAvatar = getAvatar();
    }
    else // if (getColor() == matchColor), then...
    {
        ++matchCount;
            
        if (matchCount == 3)
        {
            // We just found 3 in a row, let's set the previous 2 and this to matched...
            setMatched(true);
            west->setMatched(true);
            west->west->setMatched(true);
            
            // We want to return true...
            returnValue = true;
        }
        else if (matchCount > 3)
        {
            // Add this one
            setMatched(true);
        }
        

    }
    
    if (east) // If east is not null
    {
        // Continue finding matches
        if ( east->checkEastForMatches() ) returnValue = true;
    }
    else // We reached the end of the line, reset stuff...
    {
        //cout << "East is null";
        matchCount = 0;
        matchColor = 0;
    }
    
    return returnValue;
}

bool Space::checkSouthForMatches()
{
    PointStruct tempPointStruct;
    /*
    // debug
    int oldColor = getColor();
    setColor(15);
    draw();
    Sleep(15);
    setColor(oldColor);
    draw();
    */
    
    bool returnValue = false;
        
    if (getColor() != matchColor) // If the color is different than the last one...
    {
        if (matchCount >= 3 && !GAME_DATA->preClearing) // If so, update point queue for transfer to player's point system
        {
            PointStruct tempPointStruct(matchCount, matchColor, matchAvatar, multiplier);
            pointQueue.push(tempPointStruct);
            ++multiplier;
            
            //debugging
            //gotoxy(0, 22);
            //cout << matchAvatar << ' ';
            //system("pause");
        }
        
        matchColor = getColor(); // We're now looking for this color
        matchCount = 1; // and so far, only this space is 'matched' to itself...
        matchAvatar = getAvatar();
    }
    else if (getColor() == matchColor) // Otherwise if this space has the same color we're looking for
    {
        ++matchCount; // Increment the counter
            
        if (matchCount == 3) // If 3 spaces in a row matched...
        {
            // We just found 3 in a row, so let's set the previous 2 and this to matched...
            setMatched(true);
            north->setMatched(true);
            north->north->setMatched(true);
            
            // We want to return true since a match was found...
            returnValue = true;
        }
        else if (matchCount > 3) // Otherwise if we have over 3...
        {
            // ...just add this one
            setMatched(true);
        }
        

    }
    
    if (south) // If south is not null
    {
        // Continue checking for matches
        if ( south->checkSouthForMatches() ) returnValue = true;
        //cout << "Not null...";
    }
    else // if (!south), we reached the end of the line, reset stuff...
    {
        matchCount = 0;
        matchColor = 0;
    }
    
    return returnValue;
}

short Space::getBgColor()
{
    if (cleared)
    {
        return 0;
    }
    else return (getColor() - 8);
}

void Space::draw()
{
    
    short bgcolor = getBgColor();
    short fgcolor;
    short northbg;
    short southbg;
    
    char northCorner = 220;
    char southCorner = 223;

    (north) ? northbg = north->getBgColor() : northbg = 15;
    (south) ? southbg = south->getBgColor() : southbg = 15;          
    
    // Define the colors for the different space borders
    if (selected || highlighted)
    {
        (highlighted && selected)? fgcolor = 15 : fgcolor = 7; // i5, 7
        //if (updating) fgcolor = 8; // cannot implement without massive reworking?
    }
    else
    {
        fgcolor = bgcolor;
    }

    gotoxy(coordX, coordY);
    setcolor(fgcolor, northbg);
    cout << northCorner;
    setcolor(bgcolor, northbg);
    cout << (char)220;
    setcolor(fgcolor, northbg);
    cout << northCorner;
    
    gotoxy(coordX, coordY + 1);
    setcolor(bgcolor);       
    cout << (char)219;
    setcolor(jewel.getColor(), bgcolor);
    cout << jewel.getAvatar();
    setcolor(bgcolor);
    cout << (char)219;
    
    gotoxy(coordX, coordY + 2);
    setcolor(fgcolor, southbg);
    cout << southCorner;
    setcolor(bgcolor, southbg);
    cout << (char)223;
    setcolor(fgcolor, southbg);
    cout << southCorner;

}

Space::~Space()
{
	// Just set incrementer to 0, if one space is being destructed, they all will be!
	incrementer = 0;
}
