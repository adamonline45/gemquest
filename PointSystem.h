// PointSystem.h

#ifndef POINT_SYSTEM_H
#define POINT_SYSTEM_H

#define LIST_SIZE 13

//#include <windows.h>
#include <iostream>
#include <sstream>
#include <queue>

using namespace std;

#include "common.h"
#include "Timer.h"
#include "PointStruct.h"
#include "Jewel.h"
#include "GLOBAL_GAME_DATA.h"



class PointSystem
{
    private:

		GLOBAL_GAME_DATA *GAME_DATA;

        int offsetX;
        int offsetY;
		short width;
		short height;
        
        
        PointStruct list[LIST_SIZE]; // Holds only those being displayed...
        int latest;
        
        int total;
        
        Timer scoreTimer; // update at a given interval...
        bool halfTime; // There are a number of updates, but only one per interval...
        
        queue<PointStruct> pointQueue; // holds all points
        queue<PointStruct> poops;
        
        bool doUpdate;
    
    public:
        PointSystem();
        
        void importQueue(queue<PointStruct> newQueue);
		void addPoints(int count) { total += count; }

        void update(); // updates display
        
        void dumpInfo();

		int getPoints() { return total; }
        
        
};

#endif
