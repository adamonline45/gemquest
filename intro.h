// intro.h

// Everything from the title screen to the splash screen to the instructions are here

// This file is quite... linear.  Understandably so, because of its machinimal nature.


/*

This is the layout sample
________________________________________________________________________________ 80w


General instructions layout (slight alterations made in practice):
 ______________________________________________________________________________
|                                                                              |
|           _______________                                                    |
|          |               |                                                   |
|          |               |                           ___                     |
|          |               |                          |  w|                    |
|          | intro player  |                          |___|                    |
|          | game grid     |      moved           ___  ___  ___                |
|          |               |   <- closer ->      |  a||  d||  s|               |
|          |               |                     |___||___||___|               |
|          |               |                ________________________           |
|          |               |               |          space         |          |
|          |               |               |________________________|          |
|          |_______________|                                                   |
|                                                                              |
|               _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _                |
|              |                                               |               |
|                                                                              |
|              |              text instructions                |               |
|                                                                              |
|              |_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _|               |
|                                                                              |
|                                                                              |
|______________________________________________________________________________|

*/

#ifndef INTRO_H
#define INTRO_H

#include <conio.h>
#include <cstdlib>
#include <iostream>
using namespace std;

#include "common.h"
#include "Player.h"
#include "Sidebar.h"
#include "GLOBAL_GAME_DATA.h"

// Prototypes (keep it neat!)
void drawKey(char, bool);
void demoMove(char);

// Global introPlayer to facilitate subroutines...  Scope is only in this file, right?
Player *introPlayer;

// Lay out the offsets of each screen element
const short playerOffsetX = 17;
const short playerOffsetY = 3;
const short keyboardOffsetX = 42;
const short keyboardOffsetY = 5;
const short spacebarOffsetX = keyboardOffsetX - 3; // 3 to the left and...
const short spacebarOffsetY = keyboardOffsetY + 6; // ...6 below the keyboard coords
const short textOffsetX = 16;
const short textOffsetY = 17;
const short textWidth = 48;
const short textHeight = 5; // Must be defined, for use with the clearTextField() function...

const short instructionPageOffsetX = 58; // These are for showing the page numbers
const short instructionPageOffsetY = 4;

const short instructionPageCount = 8; // Maintain this!
short currentInstructionPage; // non-const is good 8)

// The dimensions of an ascii-graphic key
const int keyHeight = 3;
const int keyWidth = 5;
const int spaceWidth = 21;

const char keyGraphic[keyHeight][keyWidth] = {		{' ','_','_','_',' '},
													{'|',' ',' ', 1 ,'|'}, // Note the '1' hidden in here, it's used as a placeholder for the letter
													{'|','_','_','_','|'} };

const char spaceGraphic[keyHeight][spaceWidth] = {	{' ','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_',' '},
													{'|',' ',' ',' ',' ',' ',' ',' ','s','p','a','c','e',' ',' ',' ',' ',' ',' ',' ','|'},
													{'|','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','_','|'} };



// The opening screen - shows the menu
int intro()
{
    char choice = 0; // A var to store keypresses
    short current = 0; // A var to store the current menu item

    short FG0 = 0; // selected
    short BG0 = 7;
    short FG1 = 7; // deselected
    short BG1 = 0;

	Sidebar leftSidebar(1,24,15,0);
	Sidebar rightSidebar(1,24,64,0);
/*
 _  __     _     __ _ ___
| _|_ |\/|| || ||_ |_  |
|_||__|  ||_\|_||__ _| |
*/

	setcolor(7,0);
	system("CLS");
	
	leftSidebar.play();
	rightSidebar.play();

    while (choice != 13) // enter
    {

		const short menuItemCount = 5;
       
        setcolor(15, 0);
		// preoceduralize this if time exists to do so...
		gotoxy(27,4);
        cout << "       Welcome to";
		gotoxy(27,5);
		cout << " _  __     _     __ _ ___";
		gotoxy(27,6);
		cout << "| _|_ |\\/|| || ||_ |_  |";
		gotoxy(27,7);
		cout << "|_||__|  ||_\\|_||__ _| |";
        
        gotoxy(28,13);
        (current == 0) ? setcolor(FG0,BG0) : setcolor(FG1,BG1);
        cout << "    Start the Quest    " << endl;
        
        gotoxy(28,14);
        (current == 1) ? setcolor(FG0,BG0) : setcolor(FG1,BG1);
        cout << "   View High Scores    " << endl;
        
        gotoxy(28,15);
        (current == 2) ? setcolor(FG0,BG0) : setcolor(FG1,BG1);
        cout << "   View Instructions   " << endl;

		gotoxy(28,17);
        (current == 3) ? setcolor(FG0,BG0) : setcolor(FG1,BG1);
        cout << "        Options        " << endl;

		gotoxy(28,18);
        (current == 4) ? setcolor(FG0,BG0) : setcolor(FG1,BG1);
        cout << "        Credits        " << endl;

		gotoxy(28,20);
        (current == 5) ? setcolor(FG0,BG0) : setcolor(FG1,BG1);
        cout << "         Quit          " << endl;
        

		gotoxy(6,24);
		setcolor(FG0, BG0);
		cout << "- In Windows 10, enable Legacy Console Mode and use a raster font -";


        setcolor(7,0);    

        choice = getch();
        
        if (choice == (char)224) // If an arrow was pressed convert to 'wasd'
        {
            choice = getch();
            if (choice == 'H') choice = 'w';
            else if (choice == 'P') choice = 's';
        }
        
        switch(choice)
        {
            case 'w':
			case ',':
                --current;
                if (current < 0) current = menuItemCount;
                break;
            case 's':
			case 'o':
                ++current;
                if (current > menuItemCount) current = 0;
                break;
			case 27: // escape
				return 5; // the quit option
				break;
            default:
                break;
        }
    }

	system("CLS");
    return current;
}


void clearTextField()
{
	for (int y = 0; y < textHeight; ++y)
	{
		gotoxy(textOffsetX, textOffsetY + y);

		for (int x = 0; x < textWidth; ++x)
		{
			cout << ' ';
		}
	}
}

void writeInstruction(string input)
{
	// this function wraps words nicely within the textbox

	// it does not bother to take explicit line breaks within the string into account...

	int lineNumber = 0;
	int charNumber = 0;
	int beginningSpace = 0;

	// Set our ending space to the last space to start
	unsigned int endingSpace = textWidth; // unsigned to prevent drama with input.size() comparisons

	// Clear the text field
	setcolor(7,0);
	clearTextField();

	// This will get skipped if the string is shorter than the textWidth to begin with
	while ( endingSpace < input.size() )
	{
		// backtrack until we find the closest space to our line break
		while (input[endingSpace] != ' ')
		{
			--endingSpace;
		}

		gotoxy(textOffsetX, textOffsetY + lineNumber);
		cout << input.substr(beginningSpace, endingSpace - beginningSpace);
		
		beginningSpace = endingSpace + 1; // +1 to omit the space we're line-breaking on
		++lineNumber;
		endingSpace = beginningSpace + textWidth;
		
	}

	// and finish up
	gotoxy(textOffsetX, textOffsetY + lineNumber);
	cout << input.substr( beginningSpace, input.size() );
}

void updatePageNumber()
{
	++currentInstructionPage;
	setcolor(15,0);
	gotoxy(instructionPageOffsetX, instructionPageOffsetY);
	cout << currentInstructionPage << '/' << instructionPageCount;
}

void showInstructions()
{

	currentInstructionPage = 0;

	GLOBAL_GAME_DATA *GAME_DATA = GLOBAL_GAME_DATA::getInstance();

	// Set up the game data before initializing the intro player
	GAME_DATA->showingInstructions = true;
	GAME_DATA->setDimensions(5,5);
	GAME_DATA->setOffsets(playerOffsetX, playerOffsetY);
	GAME_DATA->showPoints = false;

	// instantiate our intro player
	introPlayer = new Player;

	while (GAME_DATA->preClearing)
	{
		Sleep(1);
		cout << "preclearing";
	}

	// Set the spaces to a pre-determined setup...
	introPlayer->prepareForIntro();

	// Init some sidebars
	Sidebar leftSidebar(1,24,5,0);
	Sidebar rightSidebar(1,24,74,0);

	leftSidebar.play();
	rightSidebar.play();

	// Draw the keys real quick...
	drawKey('w', false);
	drawKey('a', false);
	drawKey('s', false);
	drawKey('d', false);
	drawKey(' ', false);
//
	updatePageNumber();
	writeInstruction("This is a miniature version of the game board, which we'll use to get you acquanted with the mechanics of Gem Quest!");
	waitForEnter();
//
	updatePageNumber();
	writeInstruction("The goal of the game is to clear each space at least once. In order to clear the spaces, you need to make 3 gems in a row line up.");
	waitForEnter();
//
	updatePageNumber();
	writeInstruction("Move around using the 'w', 'a', 's', and 'd' keys.");
	waitForEnter();
//
	updatePageNumber();
	clearWaitForEnter(); // this will let the player know they have to wait for this upcoming cheesy animation to finish

	// Demonstrate the moving
	for (int x = 0; x < 4; ++x)
	{
		demoMove('s');
	}
	for (int x = 0; x < 4; ++x)
	{
		demoMove('d');
	}
	for (int x = 0; x < 4; ++x)
	{
		demoMove('w');
	}
	for (int x = 0; x < 4; ++x)
	{
		demoMove('a');
	}
	
	waitForEnter();
//	
	updatePageNumber();
	writeInstruction("Press <space> to select a gem.  Then move to an adjacent gem and press <space> again to swap them.  You can only swap neighboring gems.  Diagonal moves are not allowed.");
	waitForEnter();
//
	updatePageNumber();
	clearWaitForEnter();

	// Demonstrate a swap or two
	demoMove('d');
	demoMove('d');
	demoMove('d');
	demoMove(' ');
	demoMove('s');
	demoMove(' ');

	// Give the swap time to animate
	while (introPlayer->isUpdating())
	{
		Sleep(1);
		introPlayer->update(); // We need to send update signals to the player for the animations to play
	}

	demoMove('d');
	demoMove(' ');
	demoMove('a');
	demoMove(' ');

	// Give the swap time to animate
	while (introPlayer->isUpdating())
	{
		Sleep(1);
		introPlayer->update(); // We need to send update signals to the player for the animations to play
	}
	waitForEnter();

//
	updatePageNumber();
	writeInstruction("The goal is to clear a gem from each space on the board.  Cleared spaces have a black background; make the entire background black, and you've won!");
	waitForEnter();

//
	updatePageNumber();
	writeInstruction("That is all for now!  Have fun!");
	waitForEnter();

	// Finish up...
	delete introPlayer;
	GAME_DATA->showingInstructions = false;
	system("CLS");
}

void demoMove(char key)
{
	int x = 0;
	int y = 0;

	Timer * wait;

	if (key == 'w')
	{
		--y;
	}
	else if (key == 'a')
	{
		--x;
	}
	else if (key == 's')
	{
		++y;
	}
	else if (key == 'd')
	{
		++x;
	}
	else if (key == ' ')
	{
		introPlayer->select();
	}

	if (key != ' ') introPlayer->move(x,y);
	
	drawKey(key, true);

	wait = new Timer(200); //350
	while (!wait->signal())
	{
		Sleep(1);
	}
	delete wait;

	drawKey(key, false);
	
	wait = new Timer(200); //250
	while (!wait->signal())
	{
		Sleep(1);
	}
	delete wait;
}

void drawKey(char key, bool highlighted)
{

	if (highlighted) setcolor(15,0);
	else setcolor(8,0);

	// The offsets of the top-left of the keyboard
	int topCoord = keyboardOffsetY;
	int leftCoord = keyboardOffsetX;

	if (key == 'w')
	{
		leftCoord += keyWidth;
	}
	else if (key == 'a')
	{
		topCoord += keyHeight;
	}
	else if (key == 's')
	{
		leftCoord += keyWidth;
		topCoord += keyHeight;
	}
	else if (key == 'd')
	{
		leftCoord += keyWidth * 2;
		topCoord += keyHeight;
	}
	else if (key == ' ')
	{
		leftCoord = spacebarOffsetX;
		topCoord = spacebarOffsetY;
	}

	// draw it!
	if (key == ' ')
	{
		for (int y = 0; y < keyHeight; ++y) // keyGraphic height
		{
			gotoxy(leftCoord, topCoord + y);

			for (int x = 0; x < spaceWidth; ++x) // spaceGraphic width
			{
				cout << spaceGraphic[y][x];
			}
		}
	}
	else
	{
		for (int y = 0; y < keyHeight; ++y) // keyGraphic height
		{
			gotoxy(leftCoord, topCoord + y);

			for (int x = 0; x < keyWidth; ++x) // keyGraphic width
			{
				if (keyGraphic[y][x] == 1) cout << (char)(key - 32);  // Here we look for char 1, and replace it with the right key in CAPS
				else cout << keyGraphic[y][x];
			}
		}
	}
}


#endif