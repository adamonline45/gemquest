// Player.cpp

// deletion candidate - lib for debugging...
#include <iostream>
#include <windows.h>

#include "common.h"
#include "LoadingBar.h"

using namespace std;

#include "Player.h"

#define DEBUG 0


void Player::preClear()
{
	GAME_DATA->preClearing = true;

	setcolor(15,0);
	system("CLS");

	LoadingBar *loadingBar = new LoadingBar();

	bool originalShowPointsValue = GAME_DATA->showPoints; // problems arise with game timer?
	GAME_DATA->showPoints = false;

	while ( checkForMatches() )
	{
		clearing = true;
        updating = true;
		
		while (updating) // If the game still needs updates...
		{
			update(); // ...update it!
			loadingBar->update(); // and update the loading bar...

			// Let the loading bar finish its animation even after player's done updating
			while (!updating && !loadingBar->cycleEnded())
			{
				 loadingBar->update();
				 Sleep(1);
			}

			// This will trigger after the loading bar finishes, and the player's fully updated
			if (!updating && loadingBar->cycleEnded())
			{
				//loadingBar->remove();
			}
			
			Sleep(1);

		}

		//while(1)
		//{
		//	loadingBar->update();
		//}
	}

	GAME_DATA->preClearing = false;
	GAME_DATA->showPoints = originalShowPointsValue;

	delete loadingBar;

	system("CLS");
}

void Player::prepareForIntro()
{	
	for (int y = 0; y < GAME_DATA->height; ++y)
	{
		for (int x = 0; x < GAME_DATA->width; ++x)
		{
			// Assign 'constant' values to the jewel's color and avatar
			spaces[y][x].jewel.populate( (y * x)+(y + x) % 5);
		}
	}
	for (int y = 0; y < GAME_DATA->height; ++y)
	{
		for (int x = 0; x < GAME_DATA->width; ++x)
		{
			spaces[y][x].draw();
		}
	}
	// Redraw the first space so the cursor shows nicely...
	spaces[0][0].draw();

	//preClear();
}

// Constructor
Player::Player()
{
	GAME_DATA = GLOBAL_GAME_DATA::getInstance();

	// Create the LCD timer...
	lcd_timer = new LCD_Timer(49, 1, 5, 0, 11);
	lcd_timer->stop(); // LCD timer should default to stop, but it doesn't; this works for now tho...

	// Create the spaces
	spaces = new Space*[GAME_DATA->height];
	for (int y = 0; y < GAME_DATA->height; ++y)
	{
		spaces[y] = new Space[GAME_DATA->width];
	}

    // Fill the space pointers
    for (int y = 0; y < GAME_DATA->height; ++y)
    {
        for (int x = 0; x < GAME_DATA->width; ++x)
        {
			(x - 1 >= 0)				? spaces[y][x].west = &spaces[y][x-1]	: spaces[y][x].west = NULL;
			(x + 1 < GAME_DATA->width)	? spaces[y][x].east = &spaces[y][x+1]	: spaces[y][x].east = NULL;
			(y - 1 >= 0)				? spaces[y][x].north = &spaces[y-1][x]	: spaces[y][x].north = NULL;
			(y + 1 < GAME_DATA->height) ? spaces[y][x].south = &spaces[y+1][x]	: spaces[y][x].south = NULL;
        }
    }

	// Clear the matches that appear upon game-board creation
	preClear();
    
    // Draw them all
    for (int y = 0; y < GAME_DATA->height; ++y)
    {
        for (int x = 0; x < GAME_DATA->width; ++x)
        {
            spaces[y][x].draw();
        }   
    }    

	// Init and draw the point system
	pointSystem = new PointSystem();

	// Add white borders to the sides...
	setcolor(15,0);
	for (int y = GAME_DATA->boardOffsetY + 1; y <= (GAME_DATA->height * 2) + GAME_DATA->boardOffsetY + 1; ++y)
	{
		gotoxy(GAME_DATA->boardOffsetX, y);
		cout << (char)219;
		gotoxy(GAME_DATA->boardOffsetX + (GAME_DATA->width * 3) + 1, y);
		cout << (char)219;
	}

    // Init the values
    posX = 0;
    posY = 0;
    
    selectedPosX = -1;
    selectedPosY = -1;
    
    oldPosX = -1;
    oldPosY = -1;
    
    //points = 0;
    
    animationTimer.setPeriod(40);
    updating = false;
    dropping = false;
    lockSelections = false;
    currentFrame = 0;
    
    won = false;
	lost = false;

    move(0,0);

	// Start the timer
	if (!GAME_DATA->showingInstructions) lcd_timer->start();
}

Player::~Player()
{
	for (int y = 0; y < GAME_DATA->height; ++y)
	{
		delete []spaces[y];
	}
	delete []spaces;

	delete lcd_timer;

	delete pointSystem;

	//delete GAME_DATA; // This is BAD!  DO NOT DO!
}


bool Player::checkForWin()
{
	// Could distribute this processor load by tracking uncleared space count, updated regularly...
    for (int y = 0; y < GAME_DATA->height; ++y)
    {
        for (int x = 0; x < GAME_DATA->width; ++x)
        {
            // If there's an uncleared space, we'll return false...
            if ( !spaces[y][x].isCleared() ) return false;
        }
    }
    playerWin();
	return true;
}

void Player::playerWin()
{
	won = true;

	Timer winTimer(100); // udpate the YOU WIN text every x miliseconds
	Timer winLcdTimer(2); // update the LCD timer...

	short colors[] = {10, 10, 10, 10, 10, 2, 0, 2};
	short colorCount = 8;
	short currentColor = 0;
	const short youWonCoordX = 34;
	const short youWonCoordY = 5;
	const short winLcdTimerOffsetX = 33;
	const short winLcdTimerOffsetY = 15;
	const short winPointOffsetX = 30;
	const short winPointOffsetY = 19;

	setcolor(0,0); // black BG for cls
	system("cls");

	setcolor(7,0);
	gotoxy(25,23);
	cout << "Press <enter> to continue";

	lcd_timer->setCoords(winLcdTimerOffsetX, winLcdTimerOffsetY);
	lcd_timer->setPeriod(1); // essentially let this function's timer control the lcd timer's decrementing speed
	lcd_timer->start();


	while (1)
	{
		if (kbhit())
		{
			char choice = getch();
			if (choice == 13 && lcd_timer->getTimeLeft() <= 0) break; // <enter>
		}

		if (winTimer.signal())
		{
			(currentColor + 1 >= colorCount) ? currentColor = 0 : ++currentColor;

			setcolor(colors[currentColor], 0);
			gotoxy(youWonCoordX, youWonCoordY);
			cout << "YOU WON!";
		}

		if ( winLcdTimer.signal() && lcd_timer->getTimeLeft() > 0)
		{
			lcd_timer->update();
			pointSystem->addPoints(1);
			gotoxy(winPointOffsetX, winPointOffsetY);
			cout << pointSystem->getPoints();
		}

		Sleep(1);
	}
}


bool Player::checkForLoss()
{
	if (!won && lcd_timer->expired) // when player wins, timer goes to 0... Don't throw loss condition if game was won!
	{
		playerLost();
		return true;
	}
	return false;
}

void Player::playerLost()
{
	lost = true;

	Timer lossTimer(100); // udpate the YOU LOSE text every x miliseconds

	short colors[] = {12, 12, 12, 12, 12, 4, 0, 4};
	short colorCount = 8;
	short currentColor = 0;
	const short youLostCoordX = 33;
	const short youLostCoordY = 5;

	setcolor(7,0); // black BG for cls
	system("cls");
	
	gotoxy(25,23);
	cout << "Press <enter> to continue";

	while (1)
	{
		if (kbhit())
		{
			char choice = getch();
			if (choice == 13) break; // <enter>
		}

		if (lossTimer.signal())
		{
			(currentColor + 1 >= colorCount) ? currentColor = 0 : ++currentColor;

			setcolor(colors[currentColor], 0);
			gotoxy(youLostCoordX, youLostCoordY);
			cout << "YOU LOST!";
		}

		Sleep(1);
	}
}

void Player::update()
{
    if (updating && animationTimer.signal() )
    {
        if (clearing)
        {
            clearSpaces(); // add mult'er here?
        }
        else if (dropping)
        {
            dropSpaces();
        }
        else if (checkForMatches())
        {
            clearing = true;
        }
        else
        {
            updating = false;
			//GAME_DATA->suspendGraphicOutput = false;
            checkForWin();
        }
        if (!GAME_DATA->preClearing) spaces[posY][posX].draw(); // redraw selected space in case selections cursor got overwritten...
    }
    
    // either way update the points if it's okay...
	if (GAME_DATA->showPoints) pointSystem->update();

	// and the LCD timer
	lcd_timer->update();

	// Lastly, check the time
	checkForLoss();
}


void Player::move(int changeX, int changeY)
{
    // Store the old position
    oldPosY = posY;
    oldPosX = posX;
    
    // Set the new position
    posX += changeX;
    posY += changeY;

    // Validate the new position, revert if invalid
    if (posX > GAME_DATA->width-1 || posX < 0) posX = oldPosX;
    if (posY > GAME_DATA->height-1 || posY < 0) posY = oldPosY;
    
    // Unhighlight the old space
    spaces[oldPosY][oldPosX].setHighlighted(false);
    
    // Highlight the new space
    spaces[posY][posX].setHighlighted(true);
    
    // Redraw the old space
    spaces[oldPosY][oldPosX].draw();
    
    // Redraw the new space
    spaces[posY][posX].draw();
    
    // Redraw selected space, if it exists
    if (selectedPosX != -1) spaces[selectedPosY][selectedPosX].draw();
    
    if (DEBUG == 1)
    {
        gotoxy(50,0);
        cout << "("<<posX<<","<<posY<<")  ";
        gotoxy(50,1);
        cout << "Color: " << spaces[posY][posX].getColor() << " ";
        gotoxy(50,2);
        cout << "Empty: " << spaces[posY][posX].isEmpty();
        gotoxy(50,3);
        cout << "Matched: " << spaces[posY][posX].isMatched();
        gotoxy(50,4);
        cout << "Avatar: " << spaces[posY][posX].getAvatar() << "("<<(int)spaces[posY][posX].getAvatar()<<")";;
        gotoxy(50,5);
        cout << "Cleared: " << spaces[posY][posX].isCleared();
    }
    
}

void Player::select()
{
    if (!updating || updating)	// Don't allow movement while updating -- try without this, what happens?
								// Trying... Seems ok! :D  May need to insert something where when a space moves it becomes deselected...
    {
        
        // If they're 'reselecting' the same space, deselect it
        if (selectedPosX == posX && selectedPosY == posY)
        {
            selectedPosX = -1;
            selectedPosY = -1;
            
            spaces[posY][posX].setSelected(false);
            spaces[posY][posX].draw();
        }

		// If the newly selected space is one space either directly up or
        // down from the previously selected space...
                 // If the X is off by one...
        else if (   ((selectedPosX == posX + 1 ||
                 selectedPosX == posX - 1) &&
                 // ... and the Y is the same...
                 selectedPosY == posY)
                 ||
                 // ... or if Y is off by one...
               ((selectedPosY == posY + 1 ||
                 selectedPosY == posY - 1) &&
                 // ... and the X is the same...
                 selectedPosX == posX)
            )    
        { // ... then they're going for a match...
        
            // SWAP
            spaces[posY][posX].swap(spaces[selectedPosY][selectedPosX]);
            
            // Check for matches        
            if (checkForMatches() == false)
            {
                Sleep(500);
                // swap them back...
                spaces[posY][posX].swap(spaces[selectedPosY][selectedPosX]);
            }
            else 
            {
                clearing = true;
                updating = true;

				//GAME_DATA->suspendGraphicOutput = true;
                //update();
            }
            
            // Deselect
            selectedPosX = -1;
            selectedPosY = -1;
            
            // Redraw the highlighted one (corners can be whack)
            spaces[posY][posX].draw();
        }
        else
        { // ... otherwise wipe the old space and select the new one ...
         
            
            if (selectedPosX != -1)
            {// If there IS a space that's already been selected
            
                // Deselect old space
                spaces[selectedPosY][selectedPosX].setSelected(false);
            
                // Redraw old space
                spaces[selectedPosY][selectedPosX].draw();
            }
            
            // Set player info to reflect new space as being selected
            selectedPosX = posX;
            selectedPosY = posY;
        
            // Set new space to selected
            spaces[selectedPosY][selectedPosX].setSelected(true);
            
            // Redraw new space (keep; likely needed in the future... TODO: verify)
            spaces[selectedPosY][selectedPosX].draw();
        }
    }
    else
    {
        // cout << "Can't select at this time...";
    }
}

void Player::clearSpaces()
{   
    int counter = 0;
    for (int y = 0; y < GAME_DATA->height; ++y)
    {
        for (int x = 0; x < GAME_DATA->width; ++x)
        {
            counter += spaces[y][x].clear(currentFrame);
        }
    }
    
    ++currentFrame;
    
    if (counter > 0)
    {
        // IMPORT THE QUEUE                                 // IMPORT THE QUEUE
		if (GAME_DATA->showPoints) pointSystem->importQueue(spaces[0][0].exportQueue());
        currentFrame = 0;
        clearing = false;
        dropping = true;
    }
}

   
void Player::dropSpaces()
{
    
    int counter = 0;

    for (int x = GAME_DATA->width - 1; x >= 0; --x)
    {       
        for (int y = GAME_DATA->height - 1; y >= 0; --y)
        {
            if ( spaces[y][x].drop() )
            {
                counter++;
            }
        }
    }
    
	// When nothing has been dropped, unflag dropping state
    if (counter == 0) dropping = false; 
    
}


bool Player::checkForMatches()
{
    bool matchFound = false;
    
    // Send all the west-most spaces out searching east
    for (int y = 0; y < GAME_DATA->height; ++y)
    {
        if ( spaces[y][0].checkEastForMatches() == true) matchFound = true;
    }
    
    // Send all the north-most spaces out searching south
    for (int x = 0; x < GAME_DATA->width; ++x)
    {
        if ( spaces[0][x].checkSouthForMatches() == true) matchFound = true;
    }

    return matchFound;
}
