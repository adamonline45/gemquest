// Timer.h

#ifndef TIMER_H
#define TIMER_H

class Timer
{
    private:
        long period; // pause in ms
        long oldTime;
        long currentTime;
        
    public:
        // Constructor
        Timer();
        Timer(long period);
        void setPeriod(long);
        
        bool signal();
};

#endif
