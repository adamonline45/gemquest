// loadingBar.h
// Definition and implementation is in this file...

#ifndef LOADING_BAR_H
#define LOADING_BAR_H


#define SCREEN_WIDTH 80

// This class will surely come in handy for future projects, but will need some work...
// For now, it's designed to do what I need it to in THIS project, and is rather static...


	/*

	|||||||||| // Starting
	_____/|||| // Forward slash knocks them all down
	__________ // They're all knocked down...
	---------- // They all raise up
	\\\\\\\\\\ // Start rotating
	||||||||||
	//////////
	----------
	\\\\\\\\\\
	|||||||||| // Stop rotating, continue from beginning


	*/

#include <string>

#include "common.h"
#include "Timer.h"

class LoadingBar
{
private:
	unsigned short fgcolor;
	unsigned short bgcolor;
	unsigned short offsetX;
	unsigned short offsetY;
	unsigned short barWidth;
	unsigned short lineFrameCount;
	

	unsigned short currentLineFrame;
	unsigned short currentSpace;
	
	bool cycleComplete;

	Timer * timer;



public:
	// Constructors
	LoadingBar();
	~LoadingBar();

	void update();

	// Gets
	bool cycleEnded() { return cycleComplete; }
	

};

// Constructor
LoadingBar::LoadingBar()
{
	// Init values
	fgcolor = 15;
	bgcolor = 0;
	lineFrameCount = 8;
	barWidth = 15;
	offsetX = (SCREEN_WIDTH-barWidth)/2; // centered
	offsetY = 10;

	timer = new Timer(40);

	currentLineFrame = 0;
	currentSpace = 0;

	// Write the 'loading' text
	std::string loadingString = "LOADING";
	gotoxy( ( SCREEN_WIDTH - loadingString.size() ) / 2 , offsetY - 2); // x: centered; y: 2 above bar
	cout << loadingString;

	// Init some values
	currentLineFrame = 0;
	currentSpace = 0;

}

// Destructor
LoadingBar::~LoadingBar()
{
	delete timer;
}

void LoadingBar::update()
{
	if (timer->signal())
	{
		if (currentLineFrame == 0)
		{
			// Draw the bar
			setcolor(fgcolor,bgcolor);
			gotoxy(offsetX, offsetY);
			for (int x = 0; x < barWidth; ++x)
				cout << '|';

			cycleComplete = true;
		}
		else
		{
			cycleComplete = false;
			setcolor(fgcolor,bgcolor);
			
			if (currentLineFrame == 1)
			{
				
				if (currentSpace >= barWidth)
				{
					currentSpace = 0;
					currentLineFrame++;
				}
				else
				{
					if (currentSpace > 0)
					{
						gotoxy(offsetX + currentSpace -1, offsetY);
						cout << '_';
					}
					else gotoxy(offsetX + currentSpace, offsetY);

					if (currentSpace == barWidth - 1)
					{
						cout << '_';
						currentSpace = 0;
					}
					else
					{
						cout << '/';
						currentSpace++;
					}
				}
			}
			else if (currentLineFrame == 2)
			{
				gotoxy(offsetX, offsetY);
				for (int x = 0; x < barWidth; ++x)
					cout << '-';
			}
			else if (currentLineFrame == 3)
			{
				gotoxy(offsetX, offsetY);
				for (int x = 0; x < barWidth; ++x)
					cout << '\\';
			}
			else if (currentLineFrame == 4)
			{
				gotoxy(offsetX, offsetY);
				for (int x = 0; x < barWidth; ++x)
					cout << '|';
			}
			else if (currentLineFrame == 5)
			{
				gotoxy(offsetX, offsetY);
				for (int x = 0; x < barWidth; ++x)
					cout << '/';
			}
			else if (currentLineFrame == 6)
			{
				gotoxy(offsetX, offsetY);
				for (int x = 0; x < barWidth; ++x)
					cout << '-';
			}
			else if (currentLineFrame == 7)
			{
				gotoxy(offsetX, offsetY);
				for (int x = 0; x < barWidth; ++x)
					cout << '\\';
			}
			else if (currentLineFrame == 8)
			{
				gotoxy(offsetX, offsetY);
				for (int x = 0; x < barWidth; ++x)
					cout << '|';
			}

		}
		if (currentSpace == 0) // If there's not a line-wide sequence occuring...
		{
			(currentLineFrame >= lineFrameCount) ? currentLineFrame = 0 : ++currentLineFrame; //...increment the line frame
		}
	}
}

#endif
