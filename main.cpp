// main.cpp

// General project notes:
	

#include <cstdlib>
#include <iostream>
#include <conio.h>
#include <windows.h>

#include "Timer.h"
#include "Player.h"
#include "intro.h"
#include "highScores.h"

#include "GLOBAL_GAME_DATA.h"

using namespace std;

#define DEBUG 0

// PROTOTYPE(S)
void startGame();

int main(int argc, char *argv[])
{
	while(1)
	{
		switch(intro())
		{
			case 0:
				startGame();
				break;
			case 1:
				highScores();
				break;
			case 2:
				showInstructions();
				break;
			case 3:
				//options();
				break;
			case 4:
				// credits();
				break;
			case 5: // selected 'exit'
				return 0;
				break;
			default:
				break;
		}
	}

	return 0;
}

void startGame()
{
	GLOBAL_GAME_DATA *GAME_DATA = GLOBAL_GAME_DATA::getInstance();	

	// Set the GAME_DATA info before instantiating the player
	GAME_DATA->setDimensions(12,10);
	GAME_DATA->showPoints = true;
	GAME_DATA->setOffsets(2, 1);


    Player player1;
    
    Timer timer(1000/60); // Pulse 60x in 1000ms
    
    // Initialize a player choice variable
    char choice = 1;

    while(!player1.hasWon() && !player1.hasLost())
    {
        while (!kbhit() && !timer.signal())
        {
            Sleep(1);
        }
        
        if (kbhit())
        {
        
            choice = getch();

			if (choice == (char)224) // If an arrow was pressed convert to 'wasd'
			{
				choice = getch();
				if (choice == 'H') choice = 'w';
				else if (choice == 'P') choice = 's';
				else if (choice == 'K') choice = 'a';
				else if (choice == 'M') choice = 'd';
			}

            switch (choice)
            {
                case 'w':
				case ',':
                    player1.move(0,-1);
                    break;
                case 'a':
                    player1.move(-1,0);
                    break;
                case 'd':
				case 'e':
                    player1.move(1,0);
                    break;
                case 's':
				case 'o':
                    player1.move(0,1);
                    break;
                case ' ':
                    player1.select();
                    break;
				case 'm':
					player1.instawin();
					break;
                case 27:
                    return;
            }
        }
        else player1.update();
    }
	//GAME_DATA->suspendGraphicOutput = true;
	if ( player1.hasWon() ) checkForHighScore( player1.getScore() );

	// If player1 ends up being dynamic delete it here
}

