// sidebar.h

#ifndef SIDEBAR_H
#define SIDEBAR_H

#include <iostream>
using namespace std;

#include "Jewel.h"
#include "common.h"

class Sidebar
{

private:
	int width;
	int height;
	int offsetX;
	int offsetY;

	Jewel ** jewels;

public:
	Sidebar();
	Sidebar(int, int, int, int);
	~Sidebar();

	void clear();
	void play();
	void stop();
	void pause();
};

Sidebar::Sidebar(int w, int h, int x, int y)
{
	height = h;
	width = w;
	offsetX = x;
	offsetY = y;

	jewels = new Jewel*[height];
	for (int y = 0; y < height; ++y)
	{
		jewels[y] = new Jewel[width];
	}
}

Sidebar::~Sidebar()
{
	for (int y = 0; y < height; ++y)
	{
		delete []jewels[y]; // Hmm... Seems to work!
	
	}
	delete []jewels;
}


void Sidebar::play()
{
	for (int y = 0; y < height; ++y)
	{
		for (int x = 0; x < width; ++x)
		{
			gotoxy(offsetX + x, offsetY + y);
			setcolor(jewels[y][x].getColor());
			cout << jewels[y][x].getAvatar();
		}
	}
}

#endif