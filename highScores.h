// highScores.h



#ifndef HIGH_SCORES_H
#define HIGH_SCORES_H

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <vector>
using namespace std;

#include "common.h"

struct highScore
{
	string name;
	int score;

	friend ostream& operator << (ostream& out, highScore& scoreStruct)
	{
		out << left << setw(20) << scoreStruct.name << setw(10) << scoreStruct.score << endl;
		return out;
	}

	highScore()
	{
		name = "";
		score = 0;
	}
	highScore(string name, int score)
	{
		this->name = name;
		this->score = score;
	}
};


// Prototypes
void addHighScorer(vector<highScore>, int, int);
string getName(int, int, int);

void highScores()
{
	Sidebar leftSidebar(1,24,15,0);
	Sidebar rightSidebar(1,24,64,0);
	leftSidebar.play();
	rightSidebar.play();

	ifstream inFile;
	inFile.open("high_scores.txt");

	const short offsetX = 24;
	const short offsetY = 9;
	
	if (inFile.is_open() )
	{
	
		setcolor(15, 0);
		gotoxy(offsetX, offsetY);
		cout << left << setw(5) << "#" << setw(20) << "NAME" << setw(10) << "SCORE";
		int place = 0;

		setcolor(8, 0);
		while ( !inFile.eof() )
		{
			++ place;
			highScore tempStruct;

			getline(inFile, tempStruct.name, '#');
			inFile >> tempStruct.score;
			inFile.ignore();
			

			if (tempStruct.name != "")
			{
				gotoxy(offsetX, offsetY + place + 1); // The 1 is the hor'z gap between the heading and the scores
				cout << setw(5) << place << tempStruct;
			}
		}
		inFile.close();
	}
	else
	{
		gotoxy(offsetX, offsetY);
		cout << "No one has scored yet!";
	}

	waitForEnter();
}

void checkForHighScore(int score)
{
	ifstream inFile;
	inFile.open("high_scores.txt");

	vector<highScore> highScorers;

	if (inFile.is_open() )
	{

		// Populate the vector with existing high scores
		while ( !inFile.eof() )
		{
			highScore tempStruct;

			getline(inFile, tempStruct.name, '#');
			inFile >> tempStruct.score;
			inFile.ignore();

			if (tempStruct.name != "") highScorers.push_back(tempStruct);
		}

		inFile.close();
	}
	else
	{
		//cout << "Could not open file to read high scores" << endl;
		//system("PAUSE");
		//addHighScorer(highScorers, score, 0);
	}

	if (highScorers.size() > 0)
	{
		// Check for a high score
		for (unsigned int x = 0; x < highScorers.size(); ++x)
		{
			if (score > highScorers[x].score)
			{
				addHighScorer(highScorers, score, x);
				break; // Break out of the loop - don't want to add multiple times!
			}
		}
	}
	else // add player if no one's on the list yet
	{
		addHighScorer(highScorers, score, 0);
	}


	//waitForEnter();
}

void addHighScorer(vector<highScore> highScorers, int score, int index)
{
	// Prepare the insertion iterator
	vector<highScore>::iterator insertionIterator;
	insertionIterator = highScorers.begin();

	// Assigning the extension to the index is silly looking; place is index + 1
	string extension;
	if (index == 0) extension = "st";
	else if (index == 1) extension = "nd";
	else if (index == 2) extension = "rd";
	else if (index == 3 || index == 4) extension = "th";

	setcolor(8, 0);
	system("CLS");
	string name;
	cout << "You placed " << index + 1 << extension << "!  Please enter your name: ";
	name = getName(30,10, 18);
	//cin.ignore();

	highScore thisScorer(name, score);
	highScorers.insert(insertionIterator + index, 1, thisScorer);

	ofstream outFile;
	outFile.open("high_scores.txt");

	if (outFile.is_open() )
	{
		// write out file
		for (unsigned int x = 0; x < ( (highScorers.size() >= 5) ? 5 : highScorers.size() ); ++x)
		{
			outFile << highScorers[x].name << "#" << highScorers[x].score << endl;
		}

		outFile.close();
	}
	else
	{
		cout << "An error occured while trying to save your high score...";
		system("pause");
	}

	setcolor(7,0);
	system("CLS");
	highScores();
}

string getName(int offsetX, int offsetY, int maxChars)
{
    char *characters = new char[maxChars];
    char tempChar = 1; // Init a temp digit to hold the current character
    
    int currentIndex = 0; // Track space in the console

	gotoxy(offsetX, offsetY);
	setcolor(0,7);
	for (int x = 0; x < maxChars; ++x)
	{
		cout << ' ';
	}

    while (1)
    {
        gotoxy(offsetX + currentIndex, offsetY); 
        tempChar = getch();

        if (tempChar == 8 && currentIndex > 0) // If a backspace
        {
            --currentIndex;
            
			characters[currentIndex] = ' ';

            gotoxy(offsetX + currentIndex, offsetY);
            cout << ' ';
            gotoxy(offsetX + currentIndex, offsetY);
        }
        else if ( tempChar != 13 && currentIndex != maxChars) // If a character besides backspace or enter, add that char to array, output change
        {
            cout << tempChar; // write it to the screen
            characters[currentIndex] = tempChar; // get a num from the inputted char
            ++currentIndex;
        }
		else if (tempChar == 13) // if <enter>
		{
			stringstream ss;
			for (int i = 0; i < currentIndex; ++i)
			{
				ss << characters[i];
			}
			return ss.str();
		}
    }
	delete []characters;
}

#endif
