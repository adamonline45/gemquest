// Space.h

#ifndef SPACE_H
#define SPACE_H

#include "Jewel.h"
#include "PointStruct.h"
#include "GLOBAL_GAME_DATA.h"

#include <queue>
using std::queue;


class Space
{
    private:
		GLOBAL_GAME_DATA *GAME_DATA;

        // VARIABLES
        
        static int incrementer; // This is used to track which one is being
                                // constructed, so the x and y coordinates can
                                // be determined programatically.

        static int matchColor;  // Holds the current color while searching for matches
        static int matchCount;
        static int multiplier;
        
        static char matchAvatar;

        bool selected;      // Selected for possible swap
        bool highlighted;   // Cursor is over it
        bool matched;       // Matched, set for clearing
        bool cleared;       // Space has been cleared at least once
        bool empty;         // Space is empty...

        
    public:
        
        // The jewel
        Jewel jewel;
        
        // VARIABLES

        Space* west; // Pointer to left-hand space
        Space* east; // etc...
        Space* north;
        Space* south;
        
        // CONSTRUCTORS
        Space();       // Default
		~Space();
        
        // FUNCTIONS
        void draw();
        void swap(Space &);
        bool checkEastForMatches();
        bool checkSouthForMatches();
        int clear(int);
        bool drop();
        
        int coordX; // These are for the x and y coordinates of the top-left
        int coordY; // space's position in the console output.
        
        static queue<PointStruct> pointQueue;   // The queue
        queue<PointStruct> exportQueue();       // The function to give up the queue
        
        //bool update();
        
        
        //
        // FROM HERE DOWN, MOST FUNCTIONS DEFINED LOCALLY
        //
        
        
        // gets
        int getColor()      { return jewel.getColor();  }
        char getAvatar()    { return jewel.getAvatar(); }
        
        bool isEmpty()      { return empty;             }
        bool isMatched()    { return matched;           }
        bool isCleared()    { return cleared;           }

        int getMultiplier()      { return multiplier; multiplier = 0; }

                
        //sets
        void setHighlighted(bool value) { highlighted = value;      }
        void setSelected(bool value)    { selected = value;         }
        void setMatched(bool value)     { matched = value;          }
        void setColor(int inColor)      { jewel.setColor(inColor);  }
        void setEmpty(bool value)       { empty = value;            }
        void setCleared(bool value)     { cleared = value;          }

        short getBgColor();
        

};

#endif
