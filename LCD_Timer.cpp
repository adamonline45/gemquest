// LCD_Timer.cpp

#include "LCD_Timer.h"


LCD_Timer::LCD_Timer()
{
	// Nothing!
	timer = new Timer(1000);
}

LCD_Timer::LCD_Timer(int x, int y, int m, int s, int fg)
{
	GAME_DATA = GLOBAL_GAME_DATA::getInstance();

	expired = false;
	stopped = true; // For some reason this isn't working...

	minutes = m;
	seconds = s;
	offsetX = GAME_DATA->timerOffsetX; // makes it easy to manage...
	offsetY = GAME_DATA->timerOffsetY;
	
	fgcolor = fg;

	// One second (as fast as our clock can show updates anyway)
	timer = new Timer(1000);
	colorTimer = new Timer(100);
}

LCD_Timer::~LCD_Timer()
{
	delete timer;
}

void LCD_Timer::draw()
{
	setcolor(fgcolor, 0);

    for (int y = 0; y < 3; ++y) // One row at a time...
    {
		gotoxy(offsetX, offsetY + y);
        for (int x = 0; x < 3; ++x)
        {
            cout << characters[minutes][y][x]; // The minutes
        }

		(y > 0) ? cout << '.' : cout << ' '; // the colon

		for (int x = 0; x < 3; ++x)
        {
            cout << characters[seconds/10][y][x]; // first digit of seconds
        }

		for (int x = 0; x < 3; ++x)
        {
            cout << characters[seconds%10][y][x]; // second digit of seconds
        }
    }
}

void LCD_Timer::update()
{
	if (!expired && !stopped)
	{
		if (colorTimer->signal())
		{
			short colors[] = {10, 10, 10, 10, 10, 2, 0, 2};
			short colorCount = 8;
		}

		if (timer->signal())
		{
			if (seconds == 0)
			{
				if (minutes > 0)
				{
					--minutes;
					seconds = 59;
				}
				else expired = true;
			}
			else --seconds;

			// re-draw the timer
			draw();
		}
	}
}

const char LCD_Timer::characters[10][3][3] =
{
    {// 0
        {' ','_',' '},
        {'|',' ','|'},
        {'|','_','|'}
    },
    
    {// 1
        {' ',' ',' '},
        {' ','|',' '},
        {' ','|',' '}
    },
          
    {// 2
        {' ','_',' '},
        {' ','_','|'},
        {'|','_',' '}
    },
    
    {// 3
        {' ','_',' '},
        {' ','_','|'},
        {' ','_','|'}
    },
    
    {// 4
        {' ',' ',' '},
        {'|','_','|'},
        {' ',' ','|'}
    },
     
    {// 5
        {' ','_',' '},
        {'|','_',' '},
        {' ','_','|'}
    },

    {// 6
        {' ','_',' '},
        {'|','_',' '},
        {'|','_','|'}
    },
    
    {// 7
        {' ','_',' '},
        {' ',' ','|'},
        {' ',' ','|'}
    },
    
    {// 8
        {' ','_',' '},
        {'|','_','|'},
        {'|','_','|'}
    },
    
    {// 9
        {' ','_',' '},
        {'|','_','|'},
        {' ','_','|'}
    }
};
