// Jewel.cpp

#include <cstdlib>

#include "Jewel.h"

Jewel::Jewel()
{
    build();
}

Jewel::Jewel(int newColor, char newAvatar)
{
    color = newColor;
    avatar = newAvatar;
}

void Jewel::build()
{
    short randomNum = rand() % 5;
    
    populate(randomNum);
}

void Jewel::populate(int input)
{
    switch(input)
    {
        case 0:
            avatar = 1;
            color = 10;
            break;
        case 1:
            avatar = 2;
            color = 11;
            break;
        case 2:
			avatar = 3;
            color = 12;
            break;
        case 3:
            avatar = 4;
            color = 13;
            break;
        case 4:
            avatar = 5;
            color = 14;
            break;
    }
}

void Jewel::setColor(int color)
{
    this->color = color;
}

void Jewel::setAvatar(char newChar)
{
    avatar = newChar;   
}

